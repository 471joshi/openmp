+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

README file, Math 471, Fall 2020
Group Name: Suyog Joshi & Aashish Dhungana
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

This is the README file for the main directory.
There will be a README file for each sub-directores created(i.e. for each homework).
The README file in the sub-directories provides information about the contents and codes/reports as instructed by the professor.
HW1 is the sub-directory created in the main directory.

Homework 2

HW2 is the sub-directory created in the main directory and can be found in https://bitbucket.org/471joshi/471joshi/src/master/
Codes used for HW2 can be found inside the HW2 sub directory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW2/
README can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW2/

Homework 3

HW3 is the sub-directory created in the main directory and can be found in https://bitbucket.org/471joshi/471joshi/src/master/
There are 2 subdirectories Code and Report.
Codes used for HW3 can be found inside the Code subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW3/Code/
Report for HW3 can be found inside the Report subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW3/Report/

Homework 4

HW4 is the sub-directory created in the main directory and can be found in https://bitbucket.org/471joshi/471joshi/src/master/
There are 2 subdirectories Code and Report.
Codes used for HW4 can be found inside the Code subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW4/Code/
Report for HW4 can be found inside the Report subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW4/Report/

Homework 5

HW5 is the sub-directory created in the main directory and can be found in https://bitbucket.org/471joshi/471joshi/src/master/
There are 3 subdirectories Code, Report and Videos.
Codes used for HW5 can be found inside the Code subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW5/Code/
Report for HW5 can be found inside the Report subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW5/Report/
Videos for HW5 can be found inside the Videos subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW5/Videos/

Homework 6 

HW6 is the sub-directory created in the main directory and can be found in https://bitbucket.org/471joshi/471joshi/src/master/
There are 2 subdirectories Code and Report.
Codes used for HW6 can be found inside the Code subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW6/Code/
Report for HW6 can be found inside the Report subdirectory or can be found in https://bitbucket.org/471joshi/471joshi/src/master/HW6/Report/

